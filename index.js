// ELEMNTS
const hours = document.querySelector('.hours')
const minutes = document.querySelector('.minutes')
const seconds = document.querySelector('.seconds')
const milSeconds = document.querySelector('.milSeconds')

// BUTTONS
const startBtn = document.querySelector('.start')
const pouseBtn = document.querySelector('.pouse')
const restartBtn = document.querySelector('.restart')

// TIME
let hoursCunter = 0
let minutesCunter = 0
let secondsCunter = 0
let milSecondsCounter = 0
let pouseTimer = true

// LOCAL STORAGE DATA
const hoursCunterData = localStorage.getItem('hoursCunter')
const minutesCunterData = localStorage.getItem('minutesCunter')
const secondsCunterData = localStorage.getItem('secondsCunter')
const milSecondsCountcerData = localStorage.getItem('milSecondsCountcer')

if (milSecondsCountcerData) {
  let mlSec = +localStorage.getItem('milSecondsCountcer')
  milSecondsCounter = mlSec
  milSeconds.textContent = `${mlSec < 10 ? '0' + mlSec : mlSec}`
}

if (secondsCunterData) {
  let seSec = +localStorage.getItem('secondsCunter')
  secondsCunter = seSec
  seconds.textContent = `${seSec < 10 ? '0' + seSec : seSec}`
}

if (minutesCunterData) {
  let minut = +localStorage.getItem('secondsCunter')
  minutesCunter = minut
  minutes.textContent = `${minut < 10 ? '0' + minut : minut}`
}

if (hoursCunterData) {
  let hour = +localStorage.getItem('hoursCunter')
  hoursCunter = hour
  hours.textContent = `${hour < 10 ? '0' + hour : hour}`
}

// TIMER FUNCTION
setInterval(() => {
  if (!pouseTimer) {
    milSecondsCounter++
    localStorage.setItem('milSecondsCountcer', milSecondsCounter)
    milSeconds.textContent = `${
      milSecondsCounter < 10 ? '0' + milSecondsCounter : milSecondsCounter
    }`
    if (milSecondsCounter > 59) {
      secondsCunter++
      localStorage.setItem('secondsCunter', secondsCunter)
      seconds.textContent = `${
        secondsCunter < 10 ? '0' + secondsCunter : secondsCunter
      }`
      milSecondsCounter = 0
      if (secondsCunter > 59) {
        minutesCunter++
        localStorage.setItem('minutesCunter', minutesCunter)
        minutes.textContent = `${
          minutesCunter < 10 ? '0' + minutesCunter : minutesCunter
        }`
        secondsCunter = 0
        if (minutesCunter > 59) {
          hoursCunter++
          hours.textContent = `${
            hoursCunter < 10 ? '0' + hoursCunter : hoursCunter
          }`
          minutesCunter = 0
        }
      }
    }
    seconds.textContent == '60' ? (seconds.textContent = '00') : false
    minutes.textContent == '60' ? (minutes.textContent = '00') : false
  }
}, 16.666666666666667)

function reset() {
  hoursCunter = 0
  minutesCunter = 0
  secondsCunter = 0
  milSecondsCounter = 0
  pouseTimer = true
  milSeconds.textContent = '00'
  seconds.textContent = '00'
  minutes.textContent = '00'
  hours.textContent = '00'
  localStorage.clear()
}

// POUSE
function pouse() {
  pouseTimer = true
}

// START
function start() {
  pouseTimer = false
}

// EVENT LISTENERS
restartBtn.addEventListener('click', reset)
pouseBtn.addEventListener('click', pouse)
startBtn.addEventListener('click', start)
